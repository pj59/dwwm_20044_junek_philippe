<?php

//On souhaite développer un programme qui permet la gestion des réservations de chambre pour un hôtel qui contient 20 chambres. Les fonctions à assurer sont :
//-	Au lancement de notre programme, on crée une liste de chambres, qui sont réservées (1) ou libres (0) en utilisant la fonction randInt.
//-	Afficher l’état de l’hôtel
//-	Afficher le nombre de chambres réservées 
//-	Afficher le nombre de chambres libres  
//-	Afficher le numéro de la première chambre vide
//-	Afficher le numéro de la dernière chambre vide
//-	Réserver une chambre 
//-	Libérer une chambre


//Pour cela, nous devons afficher le menu suivant à l’utilisateur de notre programme :


//A-	Afficher l’état de l’hôtel 
//B-	Afficher le nombre de chambres réservées
//C-	Afficher le nombre de chambres libres
//D-	Afficher le numéro de la première chambre vide
//E-	Afficher le numéro de la dernière chambre vide
//F-	Réserver une chambre (Le programme doit réserver la première chambre vide)
//G-	Libérer une chambre (Le programme doit libérer la dernière chambre occupée)
//Q-    Quitter

$login = 'philippe';
$mdp = 1234;

while (true) {
	
echo ("\n");
echo ("A : Afficher l’état de l’hôtel".PHP_EOL);
echo ("B : Afficher le nombre de chambres réservées".PHP_EOL);
echo ("C : Afficher le nombre de chambres libres".PHP_EOL);
echo ("D : Afficher le numéro de la première chambre vide".PHP_EOL);
echo ("E : Afficher le numéro de la dernière chambre vide".PHP_EOL);
echo ("F : Réserver une chambre".PHP_EOL);
echo ("G : Libérer une chambre".PHP_EOL);
echo ("Q : Quitter".PHP_EOL);
echo ("\n");

$choix=readline("Veuillez faire un choix : ");


if (strtoupper($choix)=='Q') {

	echo ("Vous quittez les réservations".PHP_EOL);
	echo ("\n");
	exit;
}

if (strtoupper($choix)=='A') {

	for ($i=1;$i<=20;$i++) {
	$tab_chambres[$i] = rand(0, 1); 
	}
	for ($i=1;$i<=20;$i++) {
	  if($tab_chambres[$i] == 0)  {
		echo ("La chambre n° : ".$i. " est libre" .PHP_EOL);
	  } else {
		echo ("La chambre n° : ".$i. " est occupée" .PHP_EOL);
		
		}
	}

}


if (strtoupper($choix)=='B') {
	$cnt=0;
	for ($i=1;$i<=20;$i++) {
	  if($tab_chambres[$i] == 1)  {
		$cnt+=1;
		} 
		}
		echo ("Le nombre de chambres réservées est : ".$cnt .PHP_EOL);
		echo ("\n");
	}
	
	
if (strtoupper($choix)=='C') {
	$cnt=0;
	for ($i=1;$i<=20;$i++) {
	  if($tab_chambres[$i] == 0)  {
		$cnt+=1;
		} 
		}
		echo ("Le nombre de chambres libres est : ".$cnt .PHP_EOL);
		echo ("\n");
	}	
	
if (strtoupper($choix)=='D') {
	for ($i=1;$i<=20;$i++) {
	  if($tab_chambres[$i] == 0)  {
		echo ("Le numéro de la première chambre libre est : ".$i .PHP_EOL);
		echo ("\n");
		break;
		} 
		}
		
	}		
	
if (strtoupper($choix)=='E') {
	for ($i=20;$i>=0;$i--) {
	  if($tab_chambres[$i] == 0)  {
		echo ("Le numéro de la dernière chambre libre est : ".$i .PHP_EOL);
		echo ("\n");
		break;
		} 
		}
		
	}	
	
if (strtoupper($choix)=='F') {
	while (true) {
		$login_saisie = readline ("Veuillez saisir votre login : ");
		$mdp_saisie = readline ("Veuillez saisir votre mot de passe : ");
		echo ("\n");
		if ($login==$login_saisie and $mdp==$mdp_saisie) {
			for ($i=1;$i<=20;$i++) {
			if($tab_chambres[$i] == 0)  {
			$tab_chambres[$i]=1;  
			echo ("La reservation vient d'être faite pour la chambre n° : ".$i .PHP_EOL);
			echo ("\n");
			break;
		}
		}
		}
		
		else {
		echo ("login et/ou mot de passe incorrecte, veuillez recommencer".PHP_EOL);
		echo ("\n");
		}
		break;
		}

	}		

if (strtoupper($choix)=='G') {
	while (true) {
		$login_saisie = readline ("Veuillez saisir votre login : ");
		$mdp_saisie = readline ("Veuillez saisir votre mot de passe : ");
		echo ("\n");
		if ($login==$login_saisie and $mdp==$mdp_saisie) {
			for ($i=1;$i<=20;$i++) {
			if($tab_chambres[$i] == 1)  {
			$tab_chambres[$i]=0;  
			echo ("La chambre n° : ".$i. " vient d'être libérée" .PHP_EOL);
			echo ("\n");
			break;
		} 
		}
		}
		else {
		echo ("login et/ou mot de passe incorrecte, veuillez recommencer".PHP_EOL);
		echo ("\n");
		}
		break;
	}
	}
	
}	
?>